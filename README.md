USB MULTIBOOT
=============
Esto es una especie de manual de como tener mi pendrive multiboot.

----
#### Este contiene:
* HBCD
* Gandalf (mini Windows 10)
* Gparted ISO
* DLC Boot 2016
* Driver Pack Solution 
* 3dpChip
* 3dpNET
* Fedora 23 Net Install
* Super Grub Disk
* Super Grub Disk 2

----
### Esqueleto del pendrive:
El esqueleto es todo lo necessario para que el USB arranque:

|     MBR     | PBR 1a Particion |
| -------------- |  ------------ |
|    Grub4dos 0.4.5c  | Syslinux 4.07 |

Para hacer esto necessitaremos [BootIce](http://www.ipauly.com/wp-content/uploads/2015/11/BOOTICEx64_v1.332.rar)
 > Descargamos BootIce e instalaremos Grub4dos y Syslinux
 > ANTES de NADA Formatear la Memoria USB con Windows (la version no es relevante)
 [1]: pictures/bootice1.png
----

